/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| This file is dedicated for defining HTTP routes. A single file is enough
| for majority of projects, however you can define routes in different
| files and just make sure to import them inside this file. For example
|
| Define routes in following two files
| ├── start/routes/cart.ts
| ├── start/routes/customer.ts
|
| and then import them inside `start/routes.ts` as follows
|
| import './routes/cart'
| import './routes/customer''
|
*/

import Route from '@ioc:Adonis/Core/Route'

Route.get('/', 'PostsController.index').as('posts.index')
Route.get('/posts/create', 'PostsController.create').as('posts.create')
Route.get('/posts/edit/:id', 'PostsController.edit').as('posts.edit')
Route.post('/posts/update/:id', 'PostsController.update').as('posts.update')
Route.get('/posts/delete/:id\'', 'PostsController.delete').as('posts.delete')
Route.post('/posts/store', 'PostsController.store').as('posts.store')
