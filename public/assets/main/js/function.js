const baseUrl = ()=> {
    let str = window.location.href;
    let thesegment = str.split('/');
    let base_url = window.location.origin+'/'+thesegment[3];
    return base_url;
}

// const baseUrl = () => {
//     return window.location.protocol + '//' + window.location.host;
// }

const remove = (location, id, pesan_awal, pesan_success, pesan_error)=> {
    Swal.fire({
        title: "Apakah Anda yakin?",
        text: pesan_awal,
        icon: "warning",
        buttons: true,
        dangerMode: true,
    }).then((result) => {
        if (result.value) {
            let url = baseUrl() + location;
            let CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                url: url,
                type: "POST",
                dataType: "JSON",
                data: {_token: CSRF_TOKEN, id:id},
                beforeSend: function() {
                },
                success: function(data) {
                    if (data.status==='success') {
                        Swal.fire(
                            'Success!',
                            pesan_success,
                            'success'
                        ).then(function(){ window.location.href = '' });
                        setTimeout(function(){
                            swal.close();
                            $('.modal-delete').modal('hide');
                            window.location.href = '';
                        }, 3000);
                    } else {
                        pesan_error();
                    }
                },
                error: function(x, t, m) {
                    if (t==='timeout') {
                    } else {
                    }
                    setTimeout(function(){
                    }, 3000);
                }
            });
        } else {
        }
    });
}

const resetpassword = (location, id)=> {
    swal({
        title: "Apakah Anda yakin?",
        text: "password akan tereset ke default!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
    .then((willDelete) => {
        if (willDelete) {

            // let url = window.location.protocol + '//' + window.location.host + location;
            let url = baseUrl() + location;
            let CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                url: url,
                type: "POST",
                dataType: "JSON",
                data: {_token: CSRF_TOKEN, id:id},
                beforeSend: function() {
                },
                success: function(data) {
                    if (data.status==='success') {
                        swal("Password Berhasil Ter-reset!", {
                            icon: "success"
                        }).then(function(){
                            window.location.href = '';
                        });
                        setTimeout(function(){
                            swal.close();
                            $('.modal-delete').modal('hide');
                            window.location.href = '';
                        }, 3000);
                    } else {
                        swal("gagal reset!", {
                            icon: "failed"
                        }).then(function(){
                            window.location.href = '';
                        });
                        setTimeout(function(){
                            swal.close();
                            $('.modal-delete').modal('hide');
                            window.location.href = '';
                        }, 3000);
                    }
                },
                error: function(x, t, m) {
                    if (t==='timeout') {
                    } else {
                    }
                    setTimeout(function(){
                    }, 3000);
                }
            });

        } else {
        }
    });
}

const selectall = (selector)=> {
    $('.case').prop('checked', selector.checked);
    if ($('.case:checked').length>0) {
        $('.select-option').attr('disabled', false);
    } else {
        $('.select-option').attr('disabled', true);
    }
    var count = $('input:checked').length;
    if (count>0) {
        $('button.btn-apply').removeClass('disabled');
    } else {
        $('button.btn-apply').addClass('disabled');
    }
}

const caseclick = (selector)=> {
    if ($('.case:checked').length>0) {
        $('.select-option').attr('disabled', false);
    } else {
        $('.select-option').attr('disabled', true);
    }
    if($(".case").length == $(".case:checked").length) {
        $("#selectall").attr("checked", "checked");
    } else {
        $("#selectall").removeAttr("checked");
    }
}


/**  INSERT FUNCTION GLOBAL
* @uri adalah url controller route
* @idselector adalah selector id yang ada di <FORM>
* @pesan_success Diisi pesan setelah sukses eksekusi
* @pesan_error Diisi pesan setelah Gagal eksekusi
* @is_file apakah mengandung file atau tidak no / yes
*/
const save = (uri, selector, pesan_success, pesan_error, is_file)=> {
    $('form' + selector).on('submit', function (e){
        e.preventDefault();
        e.stopImmediatePropagation();
        let link = baseUrl() + '/' + uri;
        if (is_file=='no') {
            var formData = new FormData($(this)[0]);
        } else {
            var formData = new FormData($(this)[0]);
            var file = $('input[type=file]')[0].files[0];
            formData.append('file', file);
        }
        let inst =$(this);
        $.ajax({
            type: 'POST',
            url: link,
            data: formData,
            cache:false,
            dataType: 'JSON', /*type*/
            contentType: false,
            processData: false,
            beforeSend: function() {
                $('.btn-submit').attr('disabled', true);
            },
            success: function (data) {
                if(data.status==='success') {
                    Swal.fire(
                        'Success!',
                        pesan_success,
                        'success'
                    ).then(function(){  
                        var table = $('#datatables').DataTable();
                        table.draw();
                        $.fancybox.close();
                    });
                    setTimeout(function(){ $.fancybox.close(); }, 3000);
                } else {
                    pesan_error(data.original.error.join(', '));
                }
                $('.btn-submit').attr('disabled', false);
            },
            error: function(x, t, m) {
                pesan_error(x.original);
                if (t==='timeout') {
                } else {
                }
                $('.btn-submit').attr('disabled', false);
                setTimeout(function(){
                }, 3000);
            }
        });
    });
}

function getUrlVars()
{
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}

function getkey(e){
    if (window.event)
      return window.event.keyCode;
    else if (e)
      return e.which;
    else
      return null;
}

function angkadanhuruf(e, goods, field){
    let angka, karakterangka;
        angka = getkey(e);
    
    if (angka == null) return true;  
    karakterangka = String.fromCharCode(angka);
    karakterangka = karakterangka.toLowerCase();
    goods = goods.toLowerCase();
    // check goodkeys
    if (goods.indexOf(karakterangka) != -1)
        return true;
        // control angka
        if ( angka==null || angka==0 || angka==8 || angka==9 || angka==27 )
        return true;
            if (angka == 13){
                let i;
                for (i = 0; i < field.form.elements.length; i++)
                if (field == field.form.elements[i])
                    break;
                i = (i + 1) % field.form.elements.length;
                field.form.elements[i].focus();
                return false;
            };
        // else return false
    return false;
}