$(document).ready(function () {
    "use strict";
    $(".loader").delay(400).fadeOut();
    $(".animationload").delay(400).fadeOut("fast");
    if ($.find('#watch')[0]) {

        $('#watch').countDown({
            targetDate: {
                'day': 1,
                'month': 12,
                'year': 2019,
                'hour': 11,
                'min': 13,
                'sec': 0
            },
            omitWeeks: true
        });
        if ($('.day_field .top').html() == "0")
            $('.day_field').css('display', 'none');
    }

});





















