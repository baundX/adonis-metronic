import Post from "App/Models/Post";


export default class PostsController {
  async index({ view }) {
    const posts = await Post.all()
    return view.render('posts.index', { posts: posts })
  }

  async create({ view }) {
    return view.render('posts.create')
  }

  delete({ view }) {
    return view.render('posts.create')
  }

  async store({ request, response, session }) {
    const post = new Post()

    post.title    = request.input('title')
    post.content  = request.input('content')
    await post.save()

    session.flash({ notification: 'Data Berhasil Disimpan!' })
    response.redirect().toRoute('posts.index')

  }

  async edit({ view, params }) {
    const id    = params.id
    const post  = await Post.find(id)

    return view.render('posts.edit', { post: post })
  }

  async update({ request, response, params, session }) {
    const id    = params.id
    const post = await Post.find(id)

    // @ts-ignore
    post.title    = request.input('title')
    // @ts-ignore
    post.content  = request.input('content')
    // @ts-ignorejgjhgjgutuyt
    await post.save()

    session.flash({ notification: 'Data Berhasil Diupdate!' })
    response.redirect().toRoute('posts.index')
  }

}
